# LifetechTest

My test task for LifeTech.

List of approaches and tecnologies used:
* Kotlin
* Clean architecture
* MVVM
* LiveData
* Koin
* RxJava
* Room
