package com.antonid.domain

import io.reactivex.Single

/**
 * Repository to work with products.
 *
 * @author Ilya.Antonenko
 */
interface ProductsRepository {

    /**
     * Returns list of all products.
     *
     * @param force if true, repository will receive fresh data from network.
     *
     * @throws [Single] can throw [ProductsLoadingException] exception in 'onError'. Another exceptions are unexpected.
     */
    fun getProducts(force: Boolean = false): Single<List<Product>>

}