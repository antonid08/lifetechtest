package com.antonid.domain

/**
 * Thrown by repository when expected loading error occurred.
 *
 * @author Ilya.Antonenko
 */
class ProductsLoadingException(cause: Throwable): Exception(cause)