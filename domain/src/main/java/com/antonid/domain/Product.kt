package com.antonid.domain

class Product(val id: String, val name: String?, val price: Int?, val imageUrl: String?)