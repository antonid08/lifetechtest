package com.antonid.data.network

import com.antonid.data.repository.product.ProductsRemoteApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

internal object ApiProvider {

    val PRODUCTS_API: ProductsRemoteApi = Retrofit.Builder()
        .baseUrl("https://s3-eu-west-1.amazonaws.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(ProductsRemoteApi::class.java)

}