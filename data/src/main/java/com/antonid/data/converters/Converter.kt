package com.antonid.data.converters

interface Converter<Input, Output> {

    fun convert(input: Input): Output

}