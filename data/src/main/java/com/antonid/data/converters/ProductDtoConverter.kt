package com.antonid.data.converters

import com.antonid.data.repository.product.ProductDto
import com.antonid.domain.Product

internal class ProductDtoConverter: Converter<ProductDto, Product> {

    override fun convert(input: ProductDto): Product {
        return Product(input.id, input.name, input.price, input.imageUrl)
    }

}