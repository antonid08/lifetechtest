package com.antonid.data.repository.product

import com.antonid.data.ProductsAppDatabase
import com.antonid.data.converters.ProductDtoConverter
import com.antonid.data.network.ApiProvider
import com.antonid.domain.Product
import com.antonid.domain.ProductsLoadingException
import com.antonid.domain.ProductsRepository
import io.reactivex.Single
import retrofit2.HttpException

/**
 * Implementation of [ProductsRepository] that uses network API to manipulate the data.
 *
 * @author Ilya.Antonenko
 */
class ProductsRepositoryImpl internal constructor(
    private val database: ProductsAppDatabase,
    private val dtoConverter: ProductDtoConverter
) : ProductsRepository {

    override fun getProducts(force: Boolean): Single<List<Product>> {
        return if (force) {
            getFetchProductsFromNetworkSingle()
        } else {
            database.productsDao().getAll()
                .flatMap { localProducts ->
                    if (localProducts.isNotEmpty()) {
                        Single.just(localProducts.map { dto -> dtoConverter.convert(dto) })
                    } else {
                        getFetchProductsFromNetworkSingle()
                    }
                }
        }
    }

    private fun getFetchProductsFromNetworkSingle(): Single<List<Product>> =
        ApiProvider.PRODUCTS_API.getProducts()
            .flatMap { productResponse ->
                database.productsDao().insertAll(productResponse.products!!)
                Single.just(productResponse.products.map { dto -> dtoConverter.convert(dto) })
            }
            .onErrorResumeNext { exception ->
                when (exception) {
                    is HttpException -> Single.error(ProductsLoadingException(exception))
                    else -> Single.error(exception) //todo maybe add more expected retrofit exceptions
                }
            }

}
