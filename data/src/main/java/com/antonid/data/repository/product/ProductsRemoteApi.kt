package com.antonid.data.repository.product

import io.reactivex.Single
import retrofit2.http.GET

internal interface ProductsRemoteApi {

    @GET("developer-application-test/cart/list")
    fun getProducts(): Single<ProductsListDto>

}