package com.antonid.data.repository.product

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Product DTO entity for transferring by network and storing in database.
 *
 * @author Ilya.Antonenko
 */
@Entity
internal class ProductDto(
    @PrimaryKey @SerializedName("product_id") val id: String,
    val name: String?,
    val price: Int?,
    @SerializedName("image") val imageUrl: String?
)

/**
 * Wrapper on list of products for network API.
 *
 * @author Ilya.Antonenko
 */
internal class ProductsListDto(val products: List<ProductDto>?)
