package com.antonid.data.repository.product

import android.arch.persistence.room.*
import io.reactivex.Single

@Dao
internal interface ProductsLocalDao {

    @Query("SELECT * FROM productdto")
    fun getAll(): Single<List<ProductDto>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<ProductDto>)

    @Update
    fun update(productDto: ProductDto)

    @Query("DELETE FROM productdto")
    fun deleteAll()

}