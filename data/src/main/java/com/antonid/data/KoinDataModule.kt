package com.antonid.data

import android.arch.persistence.room.Room
import com.antonid.data.converters.ProductDtoConverter
import com.antonid.data.repository.product.ProductsRepositoryImpl
import com.antonid.domain.ProductsRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val dataModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(),
            ProductsAppDatabase::class.java, ProductsAppDatabase.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    single<ProductsRepository> { ProductsRepositoryImpl(get(), get()) }

    single { ProductDtoConverter() }

}