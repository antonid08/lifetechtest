package com.antonid.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.antonid.data.repository.product.ProductDto
import com.antonid.data.repository.product.ProductsLocalDao

@Database(entities = arrayOf(ProductDto::class), version = 1)
internal abstract class ProductsAppDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "products_database"
    }

    abstract fun productsDao(): ProductsLocalDao
}