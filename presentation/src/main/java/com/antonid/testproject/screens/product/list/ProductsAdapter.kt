package com.antonid.testproject.screens.product.list

import android.view.ViewGroup
import com.antonid.domain.Product
import com.antonid.testproject.R
import com.antonid.testproject.utils.recycler.BaseRecyclerAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.product_view_holder.view.*

/**
 * Recycler adapter for products.
 *
 * @author Ilya.Antonenko
 */
class ProductsAdapter : BaseRecyclerAdapter<Product, BaseRecyclerAdapter.ItemViewHolder<Product>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Product> {
        return ProductViewHolder(parent)
    }

    class ProductViewHolder(parent: ViewGroup) :
        BaseRecyclerAdapter.ItemViewHolder<Product>(parent, R.layout.product_view_holder) {

        override fun bind(item: Product) {
            Glide.with(itemView.context)
                .load(item.imageUrl)
                .centerInside()
                .into(itemView.image)

            itemView.name.text = item.name
            itemView.price.text = item.price.toString()
        }

    }

}