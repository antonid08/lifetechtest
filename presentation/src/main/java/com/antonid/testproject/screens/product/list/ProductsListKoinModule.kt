package com.antonid.testproject.screens.product.list

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val productsListModule = module {

    viewModel { ProductsViewModel(get()) }
}