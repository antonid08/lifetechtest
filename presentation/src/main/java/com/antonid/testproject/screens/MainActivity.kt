package com.antonid.testproject.screens

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.antonid.testproject.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
