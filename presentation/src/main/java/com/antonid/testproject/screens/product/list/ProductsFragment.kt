package com.antonid.testproject.screens.product.list

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.antonid.domain.Product
import com.antonid.domain.ProductsLoadingException
import com.antonid.testproject.Outcome
import com.antonid.testproject.R
import com.antonid.testproject.utils.handleCriticalException
import kotlinx.android.synthetic.main.products_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class ProductsFragment : Fragment() {

    companion object {
        const val PRODUCTS_IN_ROW = 2
    }

    private lateinit var productsAdapter: ProductsAdapter

    private val productsViewModel: ProductsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productsAdapter = ProductsAdapter()

        productsViewModel.productsListLiveData.observe(this, Observer { productList -> bindProducts(productList) })
        productsViewModel.loadProducts()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.products_fragment, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refresh.setOnRefreshListener(::onRefresh)

        products.layoutManager = GridLayoutManager(context, PRODUCTS_IN_ROW)
        products.adapter = productsAdapter
    }

    private fun bindProducts(productsListOutcome: Outcome<List<Product>>?) {
        when (productsListOutcome) {
            is Outcome.Success -> productsAdapter.setData(productsListOutcome.data)
            is Outcome.Progress -> setLoading(productsListOutcome.loading)
            is Outcome.Failure -> processProductsLoadingError(productsListOutcome.exception)
        }
    }

    private fun setLoading(isLoading: Boolean) {
        if (isLoading && !refresh.isRefreshing) {
            progress.visibility = View.VISIBLE
        }
        if (!isLoading) {
            progress.visibility = View.GONE
            refresh.isRefreshing = false
        }
    }

    private fun processProductsLoadingError(exception: Throwable) {
        when (exception) {
            is ProductsLoadingException ->
                Toast.makeText(context!!, R.string.loading_products_error, Toast.LENGTH_LONG).show()
            else -> handleCriticalException(exception)
        }
    }

    private fun onRefresh() {
        productsViewModel.loadProducts(true)
    }

}