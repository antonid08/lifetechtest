package com.antonid.testproject.screens.product.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.antonid.domain.Product
import com.antonid.domain.ProductsRepository
import com.antonid.testproject.Outcome
import com.antonid.testproject.utils.executeJob
import io.reactivex.disposables.CompositeDisposable

class ProductsViewModel(private val productsRepository: ProductsRepository) : ViewModel() {

    val productsListLiveData = MutableLiveData<Outcome<List<Product>>>()

    private val disposable = CompositeDisposable()

    /**
     * Loads list of products from data source
     *
     * @param force if true, fresh data will be received from network
     */
    fun loadProducts(force: Boolean = false) {
        disposable.add(executeJob(productsRepository.getProducts(force), productsListLiveData))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}