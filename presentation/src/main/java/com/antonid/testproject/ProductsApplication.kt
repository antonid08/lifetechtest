package com.antonid.testproject

import android.app.Application
import com.antonid.data.dataModule
import com.antonid.testproject.screens.product.list.productsListModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ProductsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@ProductsApplication)
            modules(listOf(productsListModule, dataModule))
        }
    }

}