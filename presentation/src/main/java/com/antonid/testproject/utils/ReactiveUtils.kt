package com.antonid.testproject.utils

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Extension function to subscribe [Single] on the IO thread and observe on the UI thread.
 * */
fun <T> Single<T>.executeOnIoThread(): Single<T> {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

/**
 * Extension function to subscribe [Observable] on the IO thread and observe on the UI thread.
 * */
fun <T> Observable<T>.executeOnIoThread(): Observable<T> {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}
