package com.antonid.testproject.utils

import android.arch.lifecycle.MutableLiveData
import com.antonid.testproject.Outcome
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/**
 * Executes a job (Single) in IO thread and manages state of executing.
 */
fun <T> executeJob(job: Single<T>, outcome: MutableLiveData<Outcome<T>>): Disposable {
    outcome.loading(true)
    return job.executeOnIoThread()
        .subscribe({
            outcome.success(it)
        }, {
            outcome.failed(it)
        })
}

/**
 * Extension function to push the loading status to the observing outcome
 * */
fun <T> MutableLiveData<Outcome<T>>.loading(isLoading: Boolean) {
    this.value = Outcome.loading(isLoading)
}

/**
 * Extension function to push  a success event with data to the observing outcome
 * */
fun <T> MutableLiveData<Outcome<T>>.success(t: T) {
    with(this) {
        loading(false)
        value = Outcome.success(t)
    }
}

/**
 * Extension function to push a failed event with an exception to the observing outcome
 * */
fun <T> MutableLiveData<Outcome<T>>.failed(e: Throwable) {
    with(this) {
        loading(false)
        value = Outcome.failure(e)
    }
}

