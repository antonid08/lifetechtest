package com.antonid.testproject.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


/**
 * Inflates view by ViewGroup
 *
 * @param layoutId id of layout for view
 * @param root     ViewGroup к которой будет добавлен View.
 */
fun inflate(root: ViewGroup, layoutId: Int): View {
    return LayoutInflater.from(root.context).inflate(layoutId, root, false)
}

/**
 * Inflates view
 *
 * @param context  android context
 * @param layoutId id of layout for view
 */
fun <T : View> inflate(context: Context, layoutId: Int): T {
    return LayoutInflater.from(context).inflate(layoutId, null) as T
}
