package com.antonid.testproject.utils.recycler


import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.antonid.testproject.utils.inflate

import java.util.ArrayList

/**
 * Base adapter for RecyclerView.
 *
 * @param <T> data type
 * @param <VH> view holder type
 *
 * @author antonenkoid
 */
abstract class BaseRecyclerAdapter<T, VH : BaseRecyclerAdapter.ItemViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private var itemsList: List<T> = ArrayList()

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun setData(@Nullable list: List<T>?) {
        itemsList = list?.let { ArrayList(it) } ?: ArrayList()
        notifyDataSetChanged()
    }

    /**
     * Clear adapter.
     */
    fun clear() {
        itemsList = ArrayList()
        notifyDataSetChanged()
    }

    private fun getItem(position: Int): T {
        return itemsList[position]
    }

    /**
     * Base view holder.
     *
     * Purposes:
     *
     *      Create ViewHolder from layout id
     *      Capability to bind model to view
     *
     * T: data type.
     */
    abstract class ItemViewHolder<T> protected constructor(parent: ViewGroup, @LayoutRes layoutId: Int) :
        RecyclerView.ViewHolder(inflate(parent, layoutId)) {

        /**
         * Binds item to itemView.
         * @param item item to bind
         */
        abstract fun bind(item: T)
    }
}

