package com.antonid.testproject.utils

/**
 * Global handler of critical errors that must crash the application.
 *
 * @author Ilya.Antonenko
 */
fun handleCriticalException(exception: Throwable) {
    throw exception
    //todo in production can don't throw exception, but print toast and send info to Fabric or something like that
}